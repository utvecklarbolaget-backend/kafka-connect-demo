# Connect configurator

The purpose of this component is to automatically configure the connector.
This is a custom implementation which can be run as a kubernetes job. It waits for the connect worker to be up and running.
The connector configuration is stored in configmaps which the connect-configurator script picks up.

## Kubernetes deployment

### Creating configmaps

Navigate to the desired config map folder (avro or json)
```sh
kubectl apply -f jdbc_sink_json_connector_configmap.yaml
kubectl apply -f jdbc_sink_avro_connector_configmap.yaml
```

### Build and push the docker image

```sh
docker image build . -t connect-configurator
docker tag connect-configurator localhost:5005/connect-configurator
docker push localhost:5005/connect-configurator
```

In case not using the local Docker Registry edit the "image:" and add the path of your image.

### Create the kubernetes job:
```sh
kubectl apply -f <connect-configurator>.yaml
```

After successfully running the job check that it has managed to configure the connector by:
* checking the logs of the job
* checking the logs of the connector
* querying the REST endpoint of the connector
