# Avro models

### Build

```sh
mvn install
```

### Register schema in schema registry

```sh
mvn io.confluent:kafka-schema-registry-maven-plugin:register
```

Note: the above registers the schemas at http://localhost:8081
In case of registering from local machine to kubernetes a port forward is needed as follows:
```sh
kubectl port-forward "<schema-registry-service>" 8081:8081
```
