#!/usr/bin/env python

import json
import argparse
import logging as log
#import uuid

from kafka import KafkaProducer
from kafka import errors
from time import sleep

TEXT_ENCODING='ascii'


def parse_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-b', '--bootstrap-server', required=True, help="e.g. 'localhost:9092'")
    parser.add_argument('-t', '--topic', required=True, help="kafka topic")
    parser.add_argument('-m', '--message', required=True, help="kafka message in json fromat")
    parser.add_argument('-n', '--number-of-messages', required=False, help="number of messages to send, default=1")
    parser.add_argument('-c', '--continuous-send', default=False, action='store_true', help="Continuously produce events.")

    return parser.parse_args()

def exit_with_error(text):
    log.info("ERROR: {0}\nExiting now.". format(text))
    exit()

class Producer:
    def __init__(self, args):
        self.bootstrap_server = args.bootstrap_server
        self.topic = args.topic
        self.message = args.message
        self.number_of_messages = 1

        if args.number_of_messages != None:
            self.number_of_messages = int(args.number_of_messages)

        try:
            self.producer = KafkaProducer(bootstrap_servers=self.bootstrap_server, value_serializer=lambda v: json.dumps(v).encode(TEXT_ENCODING))
        except errors.NoBrokersAvailable:
            exit_with_error("Broker not available at '{}'.".format(self.bootstrap_server))

    def send(self):
        log.info("Sending message '{0}'".format(self.message))
        for _ in range(self.number_of_messages):
            #self.producer.send(self.topic, key=b'{0}'.format(str(uuid.uuid4())), value=json.loads(self.message))
            self.producer.send(self.topic, value=json.loads(self.message))
        log.info("Sent {0} message(s).".format(self.number_of_messages))

def main():
    log.basicConfig(level=log.INFO)

    args = parse_args()

    producer = Producer(args)

    while True:
        producer.send()
        if not args.continuous_send:
            break

        sleep(1)


if __name__ == "__main__":
    main()
