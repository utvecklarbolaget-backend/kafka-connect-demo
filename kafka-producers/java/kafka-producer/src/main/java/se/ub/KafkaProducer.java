package se.ub;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import se.ub.product.ProductKey;
import se.ub.product.ProductUpdate;

@Component
@Slf4j
public class KafkaProducer {

    private static final String PRODUCER = "java-test-producer";
    
    private final KafkaTemplate<ProductKey, ProductUpdate> productUpdateKafkaTemplate;
    
    private final String topicName;

    @Autowired
    ProductGenerator productGenerator;

    public KafkaProducer(KafkaTemplate<ProductKey,
                        ProductUpdate> productUpdateKafkaTemplate,
                         @Value("${kafka.productupdate.topic.name}") String topicName) {
        this.productUpdateKafkaTemplate = productUpdateKafkaTemplate;
        this.topicName = topicName;
    }
    
    public void sendMessage() {
        log.info("Sending message to kafka.");
        sendProductUpdate();
    }

    public void sendMessagesContinuously() {
        while (true) {
            sendMessage();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendProductUpdate() {
        var update = productGenerator.getRandomProduct();
        productUpdateKafkaTemplate.send(topicName, new ProductKey(update.getId(), PRODUCER), update);
    }
}
