package se.ub.configuration;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import se.ub.product.ProductKey;
import se.ub.product.ProductUpdate;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties("kafka")
public class KafkaProducerConfiguration {

    @Value("${kafka.client.id}")
    private String clientId;
    @Value("${kafka.bootstrap.servers}")
    private String bootstrapServer;
    @Value("${kafka.security.protocol}")
    private String securityProtocol;

    @Value("${kafka.schema.registry.url}")
    private String schemaRegistryUrl;
    @Value("${kafka.schema.registry.basic.auth.user.info}")
    private String basicAuth;

    @Bean
    Map<String, Object> producerConfig(){
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.CLIENT_ID_CONFIG, clientId);
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        configProps.put("security.protocol", securityProtocol);
        configProps.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
        configProps.put("auto.register.schemas", false);
        configProps.put("use.latest.version", true);

        return configProps;
    }

    @Bean
    public ProducerFactory<ProductKey, ProductUpdate> productUpdateProducerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfig());
    }

    @Bean
    public KafkaTemplate<ProductKey, ProductUpdate> productUpdateKafkaTemplate() {
        return new KafkaTemplate<>(productUpdateProducerFactory());
    }
}
