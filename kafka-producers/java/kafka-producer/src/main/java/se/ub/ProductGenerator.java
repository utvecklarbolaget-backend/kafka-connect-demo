package se.ub;

import org.springframework.stereotype.Component;
import se.ub.product.ProductUpdate;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Component
public class ProductGenerator {
    public static final double minPrice = 10.0;
    public static final double maxPrice = 150.0;
    public static final List<String> productNames = Arrays.asList("pineapple", "tulips", "onion", "tv", "radio", "headphones", "keyboard", "mouse", "usb-drive", "speakers", "towel", "kiwi", "apple", "banana", "coffee", "cola", "cheese", "joghurt", "milk", "bread", "salt", "tea", "potato", "pear", "pencil", "spoon", "knife", "shirt", "battery", "soda", "eggplant", "egg", "rice", "cucumber", "orange", "melon", "chicken", "paprika", "tomato", "mushroom", "carrot", "candle");

    private String getRandomProductName() {
        Random rand = new Random();
        return productNames.get(rand.nextInt(productNames.size()));
    }

    private Double getRandomPrice() {
        Random random = new Random();
        double randomPrice = minPrice + (maxPrice - minPrice) * random.nextDouble();
        return Math.floor(randomPrice * 100) / 100;
    }

    private String getRandomId() {
        Random random = new Random();
        return String.valueOf(random.nextInt(89999999) + 10000000);
    }

    public ProductUpdate getRandomProduct() {
        return ProductUpdate.newBuilder()
                .setId(getRandomId())
                .setName(getRandomProductName())
                .setPrice(getRandomPrice())
                .build();
    }
}
