package se.ub;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@SpringBootApplication
@Configuration
@Slf4j
public class KafkaProducerApplication {

	private final ApplicationContext appContext;

	private final KafkaProducer producer;
	
	public KafkaProducerApplication(ApplicationContext appContext, KafkaProducer producer) {
		this.appContext = appContext;
		this.producer = producer;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(KafkaProducerApplication.class, args);
	}

	@PostConstruct
	void sendMessage() {
		//producer.sendMessage();
		producer.sendMessagesContinuously();
		log.info("Exit");
		System.exit(SpringApplication.exit(appContext, () -> 0));
	}
}
