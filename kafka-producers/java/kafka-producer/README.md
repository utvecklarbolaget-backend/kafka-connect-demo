# Kafka producer

Simple kafka producer implementation in Java.
It uses the avro model defined in the "avro-models" folder. That need to be built and the models have to be registered in the schema registry before proceeding with the following steps.

### Build
```sh
mvn install
```

### Run
```sh
java -jar target/kafka-producer-1.0.jar
```

### Kubernetes deployment

Build and push the docker image
```sh
docker tag kafka-producer-avro localhost:5005/kafka-producer-avro
docker push localhost:5005/kafka-producer-avro
```

Create pod
```sh
kubectl apply -f kafka_producer_avro.yaml
```
