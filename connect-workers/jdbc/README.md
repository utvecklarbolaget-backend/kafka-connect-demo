# Connect Worker

**Note: this is a separate folder for deploying the connect workers in kubernetes. For running with docker-compose check the connectors folder instead.**

## Build and push the docker image
```sh
docker tag connect-worker-jdbc localhost:5005/connect-worker-jdbc
docker push localhost:5005/connect-worker-jdbc
```

## Kubernetes deployment

### Deploy
```sh
./deploy_connect_worker.yaml
```

### Undeploy
```sh
./undeploy_connect_worker.yaml
```
