# Test of the JDBC Mysql sink with avro serialization

## Description

This includes a complete test scenario for running
* kafka broker
* kafka avro message producer
* kafka-connect JDBC Mysql sink
* mysql

for demonstrating JDBC Mysql sink in action.


## Test scenarios

1. docker-compose
2. kubernetes deployment

## 1. Instructions for the docker-compose setup

The mysql image need to be build first. Follow the instructions of the README in the "mysql" directory. Update the mysql service in the docker-compose.yml file to use the mysql image you built.

Run docker-compose:
```sh
docker-compose up -d
```

The mysql container need to be restarted at the very first time (README).  
Restart it and check its status.
```sh
docker-compose restart mysql
# look for "ready for connections" in the log
docker logs mysql -f
```

Create a database in mysql.
```sh
docker exec -it mysql bash -c 'mysql -u testuser -p'
CREATE DATABASE demo;
exit
```

Check if all containers are up and running.
```sh
docker ps
```

Also check the status of the kafka cluster at http://localhost:9021/.
If status is "Healty" then proceed with the following steps.

Create "user" topic in Kafka.
This can be done in a number of ways for example in the Control Center at http://localhost:9021/.

Wait for the connector to start up properly. Expected log is "Kafka Connect started".
```sh
docker logs kafka-connect -f
```

Configure the connector:
```sh
curl -X PUT http://localhost:8083/connectors/jdbc-mysql-sink-avro/config \
     -H "Content-Type: application/json" -d '{
    "connector.class"                    : "io.confluent.connect.jdbc.JdbcSinkConnector",
    "connection.url"                     : "jdbc:mysql://mysql:3306/demo",
    "topics"                             : "product",
    "schema.registry.url"                : "http://schema-registry:8081",
    "key.converter"                      : "io.confluent.connect.avro.AvroConverter",
    "value.converter"                    : "io.confluent.connect.avro.AvroConverter",
    "value.converter.schema.registry.url": "http://schema-registry:8081",
    "key.converter.schema.registry.url"  : "http://schema-registry:8081",
	  "key.converter.schemas.enable"       : false,
    "value.converter.schemas.enable"     : true,
    "connection.user"                    : "testuser",
    "connection.password"                : "admin",
    "auto.create"                        : true,
    "auto.evolve"                        : true,
    "insert.mode"                        : "insert",
    "pk.mode"                            : "none",
    "transforms"                         : "flatten",
    "transforms.flatten.type"            : "org.apache.kafka.connect.transforms.Flatten$Value",
    "transforms.flatten.delimiter"       : "_"
}'
```

To delete the connector
```sh
curl -X DELETE http://localhost:8083/connectors/jdbc-mysql-sink-avro
```

List the connectors:
```sh
curl -s http://localhost:8083/connectors | jq
# "jdbc-mysql-sink-avro" should appear.
# its config could be queried by:
curl -s http://localhost:8083/connectors/jdbc-mysql-sink-avro| jq
```

Produce a kafka message by running the kafka producer (kafka-producers/java folder).

Note: The schema must be registered to the schema registry first. Follow the instructions in the avro-models folder.

Check that the data is written to the "product" table.
```sh
docker exec -it mysql bash -c 'mysql -u testuser -p'
SELECT * FROM demo.product;
exit
```

## 2. kubernetes deployment

In this test setup the following components will be deployed in Kubernetes:
* kafka
* mysql
* kafka test producer (avro)
* jdbc-sink-avro connector
* connect configurator

To deploy the above components navigate to their own directory to check their README and follow the instructions to deploy.
