# Test of the JDBC Mysql sink with json serialization

## Description

This includes a complete test scenario for running
* kafka broker
* kafka json message producer
* kafka-connect JDBC Mysql sink
* mysql

for demonstrating JDBC Mysql sink in action.


## Test scenarios

1. docker-compose
2. kubernetes deployment

## 1. Instructions for the docker-compose setup

The mysql image need to be build first. Follow the instructions of the README in the "mysql" directory. Update the mysql service in the docker-compose.yml file to use the mysql image you built.

Run docker-compose:
```sh
docker-compose up -d
```

The mysql container need to be restarted at the very first time (README).  
Restart it and check its status.
```sh
docker-compose restart mysql
# look for "ready for connections" in the log
docker logs mysql -f
```

Create a database in mysql.
```sh
docker exec -it mysql bash -c 'mysql -u testuser -p'
CREATE DATABASE demo;
exit
```

Check if all containers are up and running.
```sh
docker ps
```

Also check the status of the kafka cluster at http://localhost:9021/.
If status is "Healty" then proceed with the following steps.

Create "user" topic in Kafka.
This can be done in a number of ways for example in the Control Center at http://localhost:9021/.

Wait for the connector to start up properly. Expected log is "Kafka Connect started".
```sh
docker logs kafka-connect -f
```

Configure the connector:
```sh
curl -X PUT http://localhost:8083/connectors/jdbc-mysql-sink-json/config \
     -H "Content-Type: application/json" -d '{
    "connector.class"                    : "io.confluent.connect.jdbc.JdbcSinkConnector",
    "connection.url"                     : "jdbc:mysql://mysql:3306/demo",
    "topics"                             : "user",
    "key.converter"                      : "org.apache.kafka.connect.json.JsonConverter",
    "value.converter"                    : "org.apache.kafka.connect.json.JsonConverter",
	"key.converter.schemas.enable"       : false,
	"value.converter.schemas.enable"     : true,
    "connection.user"                    : "testuser",
    "connection.password"                : "admin",
    "auto.create"                        : true,
    "auto.evolve"                        : true,
    "insert.mode"                        : "insert",
    "pk.mode"                            : "none"
}'
```

List the connectors:
```sh
curl -s http://localhost:8083/connectors | jq
# "jdbc-mysql-sink-json" should appear.
# its config could be queried by:
curl -s http://localhost:8083/connectors/jdbc-mysql-sink-json | jq
```

Produce a kafka message by running the kafka producer (kafka-producers/python folder).

Note: since we don't use schema registry in this example with the current config there must be a schema so one option is to specify it within the message.
```sh
python kafka_producer_json.py -b 'localhost:9092' -t user -m \
'{
  "schema": {
    "type": "struct", "optional": false, "version": 1, "fields": [
      { "field": "id", "type": "string", "optional": true },
      { "field": "email", "type": "string", "optional": true }
    ] },
  "payload": {
    "id": "1001",
    "email": "user@test.com"
  }
}'
```

Check that the data is written to the "user" table.
```sh
docker exec -it mysql bash -c 'mysql -u testuser -p'
SELECT * FROM demo.user;
exit
```

## 2. kubernetes deployment

In this test setup the following components will be deployed in Kubernetes:
* kafka
* mysql
* kafka test producer (json)
* jdbc-sink-json connector
* connect configurator

To deploy the above components navigate to their own directory to check their README and follow the instructions to deploy.
