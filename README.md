# Kafka Connect Demo

This demo project has a complete setup for testing Kafka Connectors with locally installed components (Kafka, ZooKeeper, connectors, producers/consumers, Mysql, Docker Registry, Kubernetes deployment etc.). It contains two test scenarios for testing JDBC sink connectors.

## Test scenarios

Tested on Mac. Docker Desktop is used for docker and kubernetes.

The two test scenarios have their own README files:
 * connectors/jdbc-sink-avro/README.md
 * connectors/jdbc-sink-json/README.md

**Those are the starting points for setting up the test scenarios (after reading this README).**

Each test scenario has
 * a local prototyping setup
 * and a kubernetes deployment
 
In the local setup docker-compose is used for running most of the components (consumers/procucers are not dockerized). Those components which have kubernetes deployment have a "deployment" folder.

Components:
 * connectors: the starting point for the test scenarios where the docker-compose based local setup is located together with the description of the kubernetes deployment
 * avro-models: a Java project for building and registering avro models
 * connect-configurator: Python script which configures the connector via REST. Deployed as a kubernetes job.
 * connect-workers: deployment of the connect workers in kubernetes
 * docker-registry: a setup for using a local docker registry
 * kafka: deployment of Kafka in kubernetes
 * kafka-consumers: kafka test consumers (avro/json)
 * kafka-producers: kafka test producers (avro/json)
 * mysql: a Mysql docker setup with automated configuration

 
