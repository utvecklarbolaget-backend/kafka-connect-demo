# Kafka consumer

Simple kafka consumer implementation in Java.
It uses the avro model defined in the "avro-models" folder. That need to be built and the models have to be registered in the schema registry before proceeding with the following steps.

### Build
```sh
mvn install
```

### Run
```sh
java -jar target/kafka-consumer-1.0.jar
```
