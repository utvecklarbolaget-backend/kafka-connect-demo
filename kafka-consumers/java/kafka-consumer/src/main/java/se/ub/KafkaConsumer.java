package se.ub;

import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import se.ub.product.ProductKey;

@Component
@Slf4j
public class KafkaConsumer {

    @KafkaListener(topics = "${kafka.productupdate.topic.name}")
    public void consume(ConsumerRecord<ProductKey, GenericRecord> record) {
        log.info("Received event: '{}'", record.value());
    }

}
