#!/usr/bin/env python

import json
import argparse

from kafka import KafkaConsumer

GROUP_ID='test-consumer-group'
CLIENT_ID='python-test-consumer'
AUTO_OFFSET_RESET='earliest'
TEXT_ENCODING='ascii'


def parse_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-b', '--bootstrap-server', required=True, help="e.g. 'localhost:9092'")

    parser.add_argument('-t', '--topic', required=True, help="kafka topic")

    parser.add_argument('-p', '--pretty-print', default=False, action='store_true', help="This enables pretty printing of the received json messages.")

    return parser.parse_args()

class Consumer:
    def __init__(self, args):
        self.bootstrap_server = args.bootstrap_server
        self.topic = args.topic
        self.pretty_print = args.pretty_print

        print("Creating KafkaConsumer.")
        self.consumer = KafkaConsumer(bootstrap_servers=self.bootstrap_server,
                                 group_id=GROUP_ID,
                                 client_id=CLIENT_ID,
                                 auto_offset_reset=AUTO_OFFSET_RESET,
                                 value_deserializer=self.json_deserializer)

        topics = [self.topic]
        print("Subscribing to topic(s) '{0}'".format(topics))
        self.consumer.subscribe(topics)
        print("Consumer created.")

    @staticmethod
    def json_deserializer(value):
        if value is None:
            print("json_deserializer: got None value")
            return

        try:
            return json.loads(value.decode(TEXT_ENCODING))
        except json.decoder.JSONDecodeError:
            print('Unable to decode json value: {0}'.format(value))
            return None
        except Exception:
            print('Unable to decode: {0}'.format(value))
            return None

    def run(self):
        print("Consuming messages:")
        for message in self.consumer:
            value = message.value

            if self.pretty_print:
                value = json.dumps(value, indent=4)
            print("Received message with key: '{0}' value:\n{1}".format(message.key, value))


def main():
    args = parse_args()

    consumer = Consumer(args)
    consumer.run()

if __name__ == "__main__":
    main()
