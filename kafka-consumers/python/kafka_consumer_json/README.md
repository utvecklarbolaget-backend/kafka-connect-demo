# Simple kafka consumer with json decoding

## Description

The script runs a kafka consumer and outputs the received messages to console.

## Usage

### Examples
```sh
python kafka_consumer_json.py -h

python kafka_consumer_json.py -b 'localhost:9092' -t 'user' -p

python kafka_consumer_json.py --bootstrap-server 'localhost:9092' --topic 'user' --pretty-print
```

### Python dependencies

kafka-python lib:
```sh
pip install kafka-python
```

Tested with Python 3.9.1

## Kubernetes deployment

Build the docker image.
```sh
docker build -t kafka-consumer-json .
```

Make sure you have your local docker-registry up and running. Follow the README file in the docker-registry folder.

Tag the image and push it to the registry.
```sh
docker tag kafka-consumer-json localhost:5005/kafka-consumer-json
docker push localhost:5005/kafka-consumer-json
```

Deploying the pod.
```sh
kubectl apply -f kafka_consumer_json.yaml
```

List the pods.
```sh
kubectl get pods
```

Print the logs of the pod.
```sh
kubectl logs kafka-consumer-json
```

Removing the pod.
```sh
kubectl delete pod kafka-consumer-json
```
