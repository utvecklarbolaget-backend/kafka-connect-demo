# MySQL databse

Simple Mysql docker image configured with a user.
The first time the image is run as a container it will setup the user 'testuser' with all privileges. 
Then it will alter the root password which will cause the container to shut down because root can't connect to the DB.

The next time the container is started everything will work just fine.

### Build
```sh
docker image build . -t <tag name>
# for our case the tag will be mysql-autoconf since we will use that name later on in the deployment files
docker image build . -t mysql-autoconf
```

### Push the image to local docker registry
```sh
docker tag mysql-autoconf localhost:5005/mysql-autoconf
docker push localhost:5005/mysql-autoconf
```

### Run
```sh
docker container run -p 3306:3306 --name <mysql-container-name> <tag name>
```
The first run will print an error and the container will be stopped
```
mysqladmin: connect to server at 'localhost' failed
error: 'Access denied for user 'root'@'localhost' (using password: YES)'
```
Start up the container again
```sh
docker container start <mysql-container-name>
```
You can now access the database as the 'testuser' via docker
```sh
docker container exec -it <mysql-container-name> mysql -utestuser -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.

```
or via jdbc 'jdbc:mysql://localhost:3306'

### Kubernetes deployment

First read the README in the deployment folder. 
```sh
cd deployment
./deploy_mysql.sh
# to remove the deployment and the service
./undeploy_mysql.sh
```
