
kubectl delete deployment zookeeper
kubectl delete deployment kafka-broker
kubectl delete deployment control-center
kubectl delete deployment schema-registry

kubectl delete service zookeeper-service
kubectl delete service kafka-service
kubectl delete service control-center-service
kubectl delete service schema-registry-service
