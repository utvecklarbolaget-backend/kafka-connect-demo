
kubectl create -f zookeeper_service.yaml
kubectl create -f kafka_broker_service.yaml 
kubectl create -f control_center_service.yaml
kubectl create -f schema_registry_service.yaml

kubectl create -f zookeeper_deployment.yaml  
kubectl create -f kafka_broker_deployment.yaml 
kubectl create -f control_center_deployment.yaml
kubectl create -f schema_registry_deployment.yaml
