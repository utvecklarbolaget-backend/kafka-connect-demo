# Kubernetes deployment of Kafka

## Deploy
```sh
./deploy_kafka.yaml
```

## Undeploy
```sh
./undeploy_kafka.yaml
```

Useful commands:

```sh
# create service
kubectl create -f <service-yaml-file>
# list services
kubectl get services
# delete service
kubectl delete service <service-name>
```

```sh
# create deployment
kubectl create -f <deployment-yaml-file>
# list deployments
kubectl get deployments
# delete deployment
kubectl delete deployment <deployment-name>
```

Port forward to be able to access services locally
```sh
kubectl port-forward <pod> <local-port>:<port-of-pod>
# example
kubectl port-forward <control-center-pod> 9021:9021
# now Control Center could be accessed on localhost:9021
```
